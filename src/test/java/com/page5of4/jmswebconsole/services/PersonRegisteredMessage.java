package com.page5of4.jmswebconsole.services;

import java.util.Date;
import java.util.UUID;

public class PersonRegisteredMessage {
   private UUID id;
   private Date dateRegistered;
   private String name;

   public UUID getId() {
      return id;
   }

   public void setId(UUID id) {
      this.id = id;
   }

   public Date getDateRegistered() {
      return dateRegistered;
   }

   public void setDateRegistered(Date dateRegistered) {
      this.dateRegistered = dateRegistered;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   protected PersonRegisteredMessage() {
      super();
   }

   public PersonRegisteredMessage(UUID id, Date dateRegistered, String name) {
      super();
      this.id = id;
      this.dateRegistered = dateRegistered;
      this.name = name;
   }
}