package com.page5of4.jmswebconsole.services;

import java.io.StringWriter;
import java.util.Date;
import java.util.UUID;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.bind.DataBindingException;
import javax.xml.bind.JAXB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestMessageFactory {
   private static final Logger logger = LoggerFactory.getLogger(TestMessageFactory.class);

   OpenMqProvider provider = new OpenMqProvider();

   public static void main(String[] args) {
      for(String address : new String[] { "127.0.0.1:7676" }) {
         new TestMessageFactory().create(address);
      }
   }

   public void create(String address) {
      try {
         Connection connection = provider.createConnection(address);
         Session session = connection.createSession(true, 0);
         for(long i = 0; i < 10; ++i) {
            enqueue(session, new PersonRegisteredMessage(UUID.randomUUID(), new Date(), "Jacob Lewallen"));
            enqueue(session, new PersonCommentedMessage(UUID.randomUUID(), "Lorem ipsum."));
         }
         session.commit();
         session.close();
         connection.close();
      }
      catch(JMSException e) {
         logger.error("Error", e);
      }
   }

   private TextMessage createMessage(Session session, Object message) {
      StringWriter writer = new StringWriter();
      try {
         JAXB.marshal(message, writer);
         return session.createTextMessage(writer.toString());
      }
      catch(DataBindingException e) {
         throw new RuntimeException("XML marshalling of message failed.", e);
      }
      catch(JMSException e) {
         throw new RuntimeException("XML marshalling of message failed.", e);
      }
   }

   public void enqueue(Session session, Object message) throws JMSException {
      enqueue(session, "default." + message.getClass().getName(), message);
   }

   public void enqueue(Session session, String name, Object message) throws JMSException {
      Queue queue = session.createQueue(name);
      MessageProducer producer = session.createProducer(queue);
      producer.send(createMessage(session, message));
      producer.close();
   }
}
