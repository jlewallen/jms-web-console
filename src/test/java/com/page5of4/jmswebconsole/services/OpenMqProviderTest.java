package com.page5of4.jmswebconsole.services;

import com.page5of4.jmswebconsole.domain.PauseType;
import com.page5of4.jmswebconsole.domain.Server;
import org.junit.Ignore;
import org.junit.Test;

@Ignore("Uses OpenMQ Server")
public class OpenMqProviderTest {
   @Test
   public void test() {
      OpenMqProvider provider = new OpenMqProvider();
      Server server = provider.createServer("127.0.0.1:7676");
      System.out.println(server);
   }

   @Test
   public void pause() {
      OpenMqProvider provider = new OpenMqProvider();
      provider.pause("127.0.0.1:7676", new String[] { "cos.outgoing.authoring.edu.wgu.support.bus.subscription.messages.SubscribeMessage" }, PauseType.NONE);
   }
}
