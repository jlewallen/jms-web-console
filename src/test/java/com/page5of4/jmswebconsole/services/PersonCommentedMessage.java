package com.page5of4.jmswebconsole.services;

import java.util.UUID;

public class PersonCommentedMessage {
   private UUID id;
   private String text;

   public UUID getId() {
      return id;
   }

   public void setId(UUID id) {
      this.id = id;
   }

   public String getText() {
      return text;
   }

   public void setText(String text) {
      this.text = text;
   }

   public PersonCommentedMessage() {
      super();
   }

   public PersonCommentedMessage(UUID id, String text) {
      super();
      this.id = id;
      this.text = text;
   }
}