package com.page5of4.jmswebconsole.services;

import javax.management.remote.JMXConnector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class JmxUtils {
   private static final Logger logger = LoggerFactory.getLogger(JmsUtils.class);

   public static void close(JMXConnector jmxc) {
      try {
         jmxc.close();
      }
      catch(Exception e) {
         logger.debug("Closing", e);
      }
   }
}
