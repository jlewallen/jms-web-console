package com.page5of4.jmswebconsole.services;

import javax.jms.Connection;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class JmsUtils {
   private static final Logger logger = LoggerFactory.getLogger(JmsUtils.class);

   public static void closeConnection(Connection connection) {
      try {
         connection.close();
      }
      catch(Exception e) {
         logger.debug("Closing", e);
      }
   }

   public static void closeSession(Session session) {
      try {
         session.close();
      }
      catch(Exception e) {
         logger.debug("Closing", e);
      }
   }
}
