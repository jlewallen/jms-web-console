package com.page5of4.jmswebconsole.services;

public class JmsServerException extends RuntimeException {
   private static final long serialVersionUID = 1L;

   public JmsServerException() {
      super();
   }

   public JmsServerException(String message, Throwable cause) {
      super(message, cause);
   }

   public JmsServerException(String message) {
      super(message);
   }

   public JmsServerException(Throwable cause) {
      super(cause);
   }
}
