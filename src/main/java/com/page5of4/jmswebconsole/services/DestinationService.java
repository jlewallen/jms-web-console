package com.page5of4.jmswebconsole.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.page5of4.jmswebconsole.domain.PauseType;
import com.page5of4.jmswebconsole.web.viewmodels.ServerViewModel;
import com.page5of4.jmswebconsole.web.viewmodels.factories.ServerViewModelFactory;

@Service
public class DestinationService {
   private final ServerProvider serverProvider;
   private final ServerViewModelFactory serverViewModelFactory;

   @Autowired
   public DestinationService(ServerProvider serverProvider, ServerViewModelFactory serverViewModelFactory) {
      super();
      this.serverProvider = serverProvider;
      this.serverViewModelFactory = serverViewModelFactory;
   }

   public ServerViewModel pause(String server, String[] names, PauseType pauseType) {
      serverProvider.pause(server, names, pauseType);
      return serverViewModelFactory.createServer(server);
   }

   public ServerViewModel purge(String server, String[] names) {
      serverProvider.purge(server, names);
      return serverViewModelFactory.createServer(server);
   }

   public ServerViewModel delete(String server, String[] names) {
      serverProvider.delete(server, names);
      return serverViewModelFactory.createServer(server);
   }
}
