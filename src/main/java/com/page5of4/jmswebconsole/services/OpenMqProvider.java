package com.page5of4.jmswebconsole.services;

import java.util.Date;
import java.util.Enumeration;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.remote.JMXConnector;

import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.page5of4.jmswebconsole.domain.Consumer;
import com.page5of4.jmswebconsole.domain.Destination;
import com.page5of4.jmswebconsole.domain.DestinationType;
import com.page5of4.jmswebconsole.domain.PauseType;
import com.page5of4.jmswebconsole.domain.Server;
import com.sun.messaging.AdminConnectionConfiguration;
import com.sun.messaging.AdminConnectionFactory;
import com.sun.messaging.ConnectionConfiguration;
import com.sun.messaging.jms.management.server.ConsumerInfo;
import com.sun.messaging.jms.management.server.ConsumerOperations;
import com.sun.messaging.jms.management.server.DestinationAttributes;
import com.sun.messaging.jms.management.server.DestinationOperations;
import com.sun.messaging.jms.management.server.DestinationPauseType;
import com.sun.messaging.jms.management.server.MQObjectName;

@Service
public class OpenMqProvider implements ServerProvider {
   public static final String DEFAULT_USERNAME = "admin";
   public static final String DEFAULT_PASSWORD = "admin";
   public static final Map<String, DestinationType> typesOut = Maps.newHashMap();
   public static final Map<DestinationType, String> typesIn = Maps.newHashMap();
   public static final Map<PauseType, String> pauseTypesIn = Maps.newHashMap();

   static {
      typesOut.put("q", DestinationType.Queue);
      typesOut.put("t", DestinationType.Topic);
      typesIn.put(DestinationType.Queue, "q");
      typesIn.put(DestinationType.Topic, "t");
      pauseTypesIn.put(PauseType.ALL, DestinationPauseType.ALL);
      pauseTypesIn.put(PauseType.CONSUMERS, DestinationPauseType.CONSUMERS);
      pauseTypesIn.put(PauseType.PRODUCERS, DestinationPauseType.PRODUCERS);
   }

   @Override
   public Server createServer(String address) {
      JMXConnector jmxc = create(address);
      Connection connection = createConnection(address);
      try {
         Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
         try {
            Server server = new Server(address, address);
            MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();
            ObjectName destinationManagerMonitorName = new ObjectName(MQObjectName.DESTINATION_MANAGER_MONITOR_MBEAN_NAME);
            ObjectName[] destinationNames = (ObjectName[])mbsc.invoke(destinationManagerMonitorName, DestinationOperations.GET_DESTINATIONS, null, null);
            for(ObjectName destinationName : destinationNames) {
               String name = (String)mbsc.getAttribute(destinationName, DestinationAttributes.NAME);
               DestinationType type = typesOut.get(mbsc.getAttribute(destinationName, DestinationAttributes.TYPE));
               Destination destination = new Destination(name);
               destination.setOldestMessageTime(getOldestMessageTime(session, name));
               destination.setType(type);
               destination.setState((String)mbsc.getAttribute(destinationName, DestinationAttributes.STATE_LABEL));
               destination.setNumberOfMessagesPendingAcks((Long)mbsc.getAttribute(destinationName, DestinationAttributes.NUM_MSGS_PENDING_ACKS));
               destination.setTotalMessagesBytes((Long)mbsc.getAttribute(destinationName, DestinationAttributes.TOTAL_MSG_BYTES));
               destination.setNumberOfActiveConsumers((Integer)mbsc.getAttribute(destinationName, DestinationAttributes.NUM_ACTIVE_CONSUMERS));
               destination.setNumberOfConsumers((Integer)mbsc.getAttribute(destinationName, DestinationAttributes.NUM_CONSUMERS));
               destination.setNumberOfProducers((Integer)mbsc.getAttribute(destinationName, DestinationAttributes.NUM_PRODUCERS));
               destination.setNumberOfMessages((Long)mbsc.getAttribute(destinationName, DestinationAttributes.NUM_MSGS));
               destination.setNumberOfMessagesIn((Long)mbsc.getAttribute(destinationName, DestinationAttributes.NUM_MSGS_IN));
               destination.setNumberOfMessagesOut((Long)mbsc.getAttribute(destinationName, DestinationAttributes.NUM_MSGS_OUT));
               server.getDestinations().add(destination);
            }
            ObjectName consumerManagerMonitorName = new ObjectName(MQObjectName.CONSUMER_MANAGER_MONITOR_MBEAN_NAME);
            CompositeData[] cdArray = (CompositeData[])mbsc.invoke(consumerManagerMonitorName, ConsumerOperations.GET_CONSUMER_INFO, null, null);
            if(cdArray != null) {
               for(CompositeData cd : cdArray) {
                  Consumer consumer = new Consumer();
                  consumer.setConsumerId((String)cd.get(ConsumerInfo.CONSUMER_ID));
                  consumer.setUser((String)cd.get(ConsumerInfo.USER));
                  consumer.setHost((String)cd.get(ConsumerInfo.HOST));
                  consumer.setServiceName((String)cd.get(ConsumerInfo.SERVICE_NAME));
                  consumer.setDestinationName((String)cd.get(ConsumerInfo.DESTINATION_NAME));
                  consumer.setDestinationType(typesOut.get(cd.get(ConsumerInfo.DESTINATION_TYPE)));
                  consumer.setLastAcknowledgeTime(new Date((Long)cd.get(ConsumerInfo.LAST_ACK_TIME)));
                  consumer.setNumberOfMessages((Long)cd.get(ConsumerInfo.NUM_MSGS));
                  consumer.setNumberOfMessagesPendingAcks((Long)cd.get(ConsumerInfo.NUM_MSGS_PENDING_ACKS));
                  consumer.setPaused((Boolean)cd.get(ConsumerInfo.FLOW_PAUSED));
                  server.getConsumers().add(consumer);
               }
            }
            return server;
         }
         finally {
            JmsUtils.closeSession(session);
         }
      }
      catch(Exception e) {
         throw new JmsServerException(e);
      }
      finally {
         JmsUtils.closeConnection(connection);
         JmxUtils.close(jmxc);
      }
   }

   private DateTime getOldestMessageTime(Session session, String name) throws JMSException {
      QueueBrowser browser = session.createBrowser(session.createQueue(name));
      Enumeration<?> enumeration = browser.getEnumeration();
      if(enumeration.hasMoreElements()) {
         Message message = (Message)enumeration.nextElement();
         return new DateTime(message.getJMSTimestamp());
      }
      return null;
   }

   @Override
   public void pause(String address, String[] names, PauseType pauseType) {
      JMXConnector jmxc = create(address);
      try {
         MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();
         for(String name : names) {
            ObjectName destinationName = MQObjectName.createDestinationConfig(typesIn.get(DestinationType.Queue), name);
            if(pauseType == PauseType.NONE) {
               mbsc.invoke(destinationName, DestinationOperations.RESUME, new Object[] {}, new String[] {});
            }
            else {
               mbsc.invoke(destinationName, DestinationOperations.PAUSE, new Object[] { pauseTypesIn.get(pauseType) }, new String[] { String.class.getName() });
            }
         }
      }
      catch(Exception e) {
         throw new JmsServerException(e);
      }
      finally {
         JmxUtils.close(jmxc);
      }
   }

   @Override
   public void purge(String address, String[] names) {
      JMXConnector jmxc = create(address);
      try {
         MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();
         for(String name : names) {
            ObjectName destinationName = MQObjectName.createDestinationConfig(typesIn.get(DestinationType.Queue), name);
            mbsc.invoke(destinationName, DestinationOperations.PURGE, new Object[] {}, new String[] {});
         }
      }
      catch(Exception e) {
         throw new JmsServerException(e);
      }
      finally {
         JmxUtils.close(jmxc);
      }
   }

   @Override
   public void delete(String address, String[] names) {
      JMXConnector jmxc = create(address);
      try {
         MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();
         ObjectName destinationManagerName = new ObjectName(MQObjectName.DESTINATION_MANAGER_CONFIG_MBEAN_NAME);
         for(String name : names) {
            mbsc.invoke(destinationManagerName, DestinationOperations.DESTROY, new Object[] { "q", name }, new String[] { String.class.getName(), String.class.getName() });
         }
      }
      catch(Exception e) {
         throw new JmsServerException(e);
      }
      finally {
         JmxUtils.close(jmxc);
      }
   }

   private JMXConnector create(String address) {
      try {
         AdminConnectionFactory adminConnectionFactory = new AdminConnectionFactory();
         adminConnectionFactory.setProperty(AdminConnectionConfiguration.imqAddress, address);
         return adminConnectionFactory.createConnection(DEFAULT_USERNAME, DEFAULT_PASSWORD);
      }
      catch(Exception e) {
         throw new JmsServerException(e);
      }
   }

   @Override
   public Connection createConnection(String address) {
      try {
         com.sun.messaging.ConnectionFactory factory = new com.sun.messaging.XAConnectionFactory();
         factory.setProperty(ConnectionConfiguration.imqAddressList, "mq://" + address);
         factory.setProperty(ConnectionConfiguration.imqReconnectEnabled, "true");
         factory.setProperty(ConnectionConfiguration.imqReconnectAttempts, "5");
         factory.setProperty(ConnectionConfiguration.imqReconnectInterval, "500");
         factory.setProperty(ConnectionConfiguration.imqAddressListBehavior, "RANDOM");
         return factory.createConnection();
      }
      catch(Exception ex) {
         throw new RuntimeException("Error setting Address: " + address, ex);
      }
   }
}
