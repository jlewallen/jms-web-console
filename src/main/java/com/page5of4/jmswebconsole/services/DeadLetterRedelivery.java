package com.page5of4.jmswebconsole.services;

import javax.jms.Connection;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeadLetterRedelivery {
   private final ServerProvider provider;

   @Autowired
   public DeadLetterRedelivery(ServerProvider provider) {
      super();
      this.provider = provider;
   }

   public void redeliver(QueueAddress from, QueueAddress to, long number) {
      Connection fromConnection = provider.createConnection(from.getServer());
      Connection toConnection = provider.createConnection(to.getServer());
      try {
         fromConnection.start();
         toConnection.start();
         Session fromSession = fromConnection.createSession(true, 0);
         Session toSession = toConnection.createSession(true, 0);
         try {
            MessageConsumer consumer = fromSession.createConsumer(fromSession.createQueue(from.getName()));
            MessageProducer producer = toSession.createProducer(toSession.createQueue(to.getName()));
            while(number-- > 0) {
               Message message = consumer.receive(500);
               if(message == null) {
                  break;
               }
               producer.send(message);
            }
            producer.close();
            consumer.close();
            toSession.commit();
            fromSession.commit();
         }
         finally {
            JmsUtils.closeSession(fromSession);
            JmsUtils.closeSession(toSession);
         }
      }
      catch(Throwable e) {
         throw new RuntimeException(e);
      }
      finally {
         JmsUtils.closeConnection(fromConnection);
         JmsUtils.closeConnection(toConnection);
      }
   }
}
