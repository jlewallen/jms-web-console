package com.page5of4.jmswebconsole.services;

import javax.annotation.PostConstruct;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestQueuesCreator {
   private static final Logger logger = LoggerFactory.getLogger(TestQueuesCreator.class);
   private final ServerProvider provider;

   public TestQueuesCreator(ServerProvider provider) {
      super();
      this.provider = provider;
   }

   @PostConstruct
   public void create() {
      try {
         Connection connection = provider.createConnection("127.0.0.1:7676");
         Session session = connection.createSession(true, 0);
         createQueue(session, "whatever.com.page5of4.jmswebconsole.messages.SomethingHappenedMessage");
         createQueue(session, "whatever.com.page5of4.jmswebconsole.messages.SomethingHappenedMessage.deadLetter");
         createQueue(session, "whatever.outgoing.whatever.com.page5of4.jmswebconsole.messages.SomethingHappenedMessage");
         session.commit();
         session.close();
         connection.close();
      }
      catch(JMSException e) {
         logger.error("Error", e);
      }
   }

   public void createQueue(Session session, String name) throws JMSException {
      Queue queue = session.createQueue(name);
      MessageProducer producer = session.createProducer(queue);
      producer.send(session.createTextMessage("Hello, world!"));
      producer.close();
   }
}
