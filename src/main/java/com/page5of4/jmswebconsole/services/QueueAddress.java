package com.page5of4.jmswebconsole.services;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class QueueAddress {
   private final String server;
   private final String name;

   public String getServer() {
      return server;
   }

   public String getName() {
      return name;
   }

   public QueueAddress(String server, String name) {
      super();
      this.server = server;
      this.name = name;
   }

   @Override
   public int hashCode() {
      return HashCodeBuilder.reflectionHashCode(this);
   }

   @Override
   public boolean equals(Object other) {
      return EqualsBuilder.reflectionEquals(this, other);
   }

   @Override
   public String toString() {
      return name + "@" + server;
   }

   public static QueueAddress parse(String text) {
      String[] parts = text.split("@");
      if(parts.length != 2) {
         throw new RuntimeException("Error parsing " + text + " as QueueAddress");
      }
      return new QueueAddress(parts[1], parts[0]);
   }
}
