package com.page5of4.jmswebconsole.services;

import javax.jms.Connection;

import com.page5of4.jmswebconsole.domain.PauseType;
import com.page5of4.jmswebconsole.domain.Server;

public interface ServerProvider {
   Server createServer(String address);

   void pause(String address, String[] names, PauseType pauseType);

   void purge(String address, String[] names);

   void delete(String address, String[] names);

   Connection createConnection(String address);
}
