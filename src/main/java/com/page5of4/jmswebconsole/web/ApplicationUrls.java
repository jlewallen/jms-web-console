package com.page5of4.jmswebconsole.web;

import java.net.URLEncoder;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

import org.springframework.web.context.ServletConfigAware;

public class ApplicationUrls implements ServletConfigAware {
   private static Encoder encoder = new FakeEncoder("/application");

   public static String encode(String url) {
      return encoder.encode(url);
   }

   public static String encode(String url, Object... args) {
      try {
         Object[] encoded = new Object[args.length];
         for(int i = 0; i < args.length; ++i) {
            encoded[i] = URLEncoder.encode(args[i].toString(), "UTF-8");
         }
         return encode(String.format(url, encoded));
      }
      catch(Exception e) {
         throw new RuntimeException(e);
      }
   }

   @Override
   public void setServletConfig(ServletConfig servletConfig) {
      encoder = new ServletEncoder(servletConfig.getServletContext());
   }

   public static interface Encoder {
      public String encode(String url);
   }

   public static class FakeEncoder implements Encoder {
      private final String prefix;

      public FakeEncoder(String prefix) {
         super();
         this.prefix = prefix;
      }

      @Override
      public String encode(String url) {
         return prefix + url;
      }
   }

   public static class ServletEncoder implements Encoder {
      private final ServletContext servletContext;

      public ServletEncoder(ServletContext servletContext) {
         super();
         this.servletContext = servletContext;
      }

      @Override
      public String encode(String url) {
         return servletContext.getContextPath() + url;
      }
   }
}
