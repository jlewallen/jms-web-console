package com.page5of4.jmswebconsole.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.page5of4.jmswebconsole.web.viewmodels.factories.ServerListingViewModelFactory;
import com.page5of4.jmswebconsole.web.viewmodels.factories.ServerViewModelFactory;
import com.page5of4.mustache.SingleModelAndView;

@Controller
@RequestMapping(value = "servers")
public class ServerController {
   private final ServerViewModelFactory serverViewModelFactory;
   private final ServerListingViewModelFactory serverListingViewModelFactory;

   @Autowired
   public ServerController(ServerViewModelFactory serverViewModelFactory, ServerListingViewModelFactory serverListingViewModelFactory) {
      super();
      this.serverViewModelFactory = serverViewModelFactory;
      this.serverListingViewModelFactory = serverListingViewModelFactory;
   }

   @RequestMapping
   public ModelAndView servers() {
      return new SingleModelAndView("empty", serverListingViewModelFactory.createServerListing());
   }

   @RequestMapping(params = "address")
   public ModelAndView server(@RequestParam String address) {
      return new SingleModelAndView("empty", serverViewModelFactory.createServer(address));
   }
}
