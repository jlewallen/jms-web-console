package com.page5of4.jmswebconsole.web;

import com.page5of4.jmswebconsole.services.QueueAddress;

public class Urls {
   public static String makeServerUrl(String address) {
      return ApplicationUrls.encode("/servers?address=%s", address);
   }

   public static String makeDestinationUrl(String address, String name) {
      return ApplicationUrls.encode("/destinations.json?server=%s&name=%s", address, name);
   }

   public static String makePauseDestinationUrl(String address, String name) {
      return ApplicationUrls.encode("/destinations/pause.json?server=%s&name=%s", address, name);
   }

   public static String makeDeleteDestinationUrl(String address, String name) {
      return ApplicationUrls.encode("/destinations/delete.json?server=%s&name=%s", address, name);
   }

   public static String makePurgeDestinationUrl(String address, String name) {
      return ApplicationUrls.encode("/destinations/purge.json?server=%s&name=%s", address, name);
   }

   public static String makePauseDestinationUrl(String address) {
      return ApplicationUrls.encode("/destinations/pause.json?server=%s", address);
   }

   public static String makeDeleteDestinationUrl(String address) {
      return ApplicationUrls.encode("/destinations/delete.json?server=%s", address);
   }

   public static String makePurgeDestinationUrl(String address) {
      return ApplicationUrls.encode("/destinations/purge.json?server=%s", address);
   }

   public static String makeRedeliverUrl(QueueAddress from, QueueAddress to) {
      return ApplicationUrls.encode("/destinations/redeliver.json?from=%s&to=%s", from.toString(), to.toString());
   }

   public static String makeDestinationMessagesUrl(String address, String name) {
      return ApplicationUrls.encode("/destinations/messages.json?server=%s&name=%s", address, name);
   }

   public static String makeServersUrl() {
      return ApplicationUrls.encode("/servers.json");
   }
}
