package com.page5of4.jmswebconsole.web.viewmodels;

import java.util.List;

public class ServerListingViewModel {
   private final List<ServerListingServerViewModel> servers;

   public List<ServerListingServerViewModel> getServers() {
      return servers;
   }

   public ServerListingViewModel(List<ServerListingServerViewModel> servers) {
      this.servers = servers;
   }
}
