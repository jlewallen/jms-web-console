package com.page5of4.jmswebconsole.web.viewmodels;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.joda.time.DateTime;
import org.joda.time.Duration;

public class MessageViewModel {
   private final String id;
   private final DateTime dateSent;
   private final String payload;

   public String getId() {
      return id;
   }

   @JsonSerialize(using = DateTimeSerializer.class)
   public DateTime getDateSent() {
      return dateSent;
   }

   public long getAgeInSeconds() {
      Duration duration = new Duration(dateSent, new DateTime());
      return duration.getStandardSeconds();
   }

   public String getPayload() {
      return payload;
   }

   public MessageViewModel(String id, DateTime dateSent, String payload) {
      super();
      this.id = id;
      this.dateSent = dateSent;
      this.payload = payload;
   }
}
