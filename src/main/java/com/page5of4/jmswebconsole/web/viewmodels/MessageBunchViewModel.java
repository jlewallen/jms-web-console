package com.page5of4.jmswebconsole.web.viewmodels;

import java.util.List;

public class MessageBunchViewModel {
   private final List<MessageViewModel> messages;

   public List<MessageViewModel> getMessages() {
      return messages;
   }

   public MessageBunchViewModel(List<MessageViewModel> messages) {
      super();
      this.messages = messages;
   }
}
