package com.page5of4.jmswebconsole.web.viewmodels;

import java.util.List;

import com.google.common.collect.Lists;
import com.page5of4.jmswebconsole.web.Urls;

public class ServerViewModel {
   private String address;
   private String name;
   private final String purgeUrl;
   private final String pauseUrl;
   private final String deleteUrl;
   private final List<DestinationViewModel> destinations = Lists.newArrayList();
   private final List<ConsumerViewModel> consumers = Lists.newArrayList();

   public String getAddress() {
      return address;
   }

   public void setAddress(String address) {
      this.address = address;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getPurgeUrl() {
      return purgeUrl;
   }

   public String getPauseUrl() {
      return pauseUrl;
   }

   public String getDeleteUrl() {
      return deleteUrl;
   }

   public ServerViewModel(String address, String name) {
      super();
      this.address = address;
      this.name = name;
      this.purgeUrl = Urls.makePurgeDestinationUrl(address);
      this.deleteUrl = Urls.makeDeleteDestinationUrl(address);
      this.pauseUrl = Urls.makePauseDestinationUrl(address);
   }

   public List<DestinationViewModel> getDestinations() {
      return destinations;
   }

   public List<ConsumerViewModel> getConsumers() {
      return consumers;
   }
}
