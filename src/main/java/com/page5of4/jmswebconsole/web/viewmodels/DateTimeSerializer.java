package com.page5of4.jmswebconsole.web.viewmodels;

import java.io.IOException;
import java.text.SimpleDateFormat;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.joda.time.DateTime;

public class DateTimeSerializer extends JsonSerializer<DateTime> {
   private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("MM-dd-yyyy HH:MM:SS");

   @Override
   public void serialize(DateTime value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
      jgen.writeString(FORMATTER.format(value.toDate()));
   }
}
