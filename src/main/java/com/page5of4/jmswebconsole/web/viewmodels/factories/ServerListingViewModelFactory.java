package com.page5of4.jmswebconsole.web.viewmodels.factories;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.page5of4.jmswebconsole.domain.Server;
import com.page5of4.jmswebconsole.domain.repositories.ServerRepository;
import com.page5of4.jmswebconsole.web.Urls;
import com.page5of4.jmswebconsole.web.viewmodels.ServerListingServerViewModel;
import com.page5of4.jmswebconsole.web.viewmodels.ServerListingViewModel;

@Service
public class ServerListingViewModelFactory {
   private final ServerRepository serverRepository;

   @Autowired
   public ServerListingViewModelFactory(ServerRepository serverRepository) {
      super();
      this.serverRepository = serverRepository;
   }

   public ServerListingViewModel createServerListing() {
      List<ServerListingServerViewModel> servers = Lists.newArrayList();
      for(Server server : serverRepository.findAll()) {
         servers.add(new ServerListingServerViewModel(server.getAddress(), server.getName(), Urls.makeServerUrl(server.getAddress())));
      }
      return new ServerListingViewModel(servers);
   }
}
