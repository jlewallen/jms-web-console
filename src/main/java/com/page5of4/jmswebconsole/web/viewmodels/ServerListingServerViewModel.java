package com.page5of4.jmswebconsole.web.viewmodels;

public class ServerListingServerViewModel {
   private final String address;
   private final String name;
   private final String url;

   public String getAddress() {
      return address;
   }

   public String getName() {
      return name;
   }

   public String getUrl() {
      return url;
   }

   public ServerListingServerViewModel(String address, String name, String url) {
      this.address = address;
      this.name = name;
      this.url = url;
   }
}
