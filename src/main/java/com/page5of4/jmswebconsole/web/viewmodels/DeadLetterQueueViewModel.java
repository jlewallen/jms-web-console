package com.page5of4.jmswebconsole.web.viewmodels;

public class DeadLetterQueueViewModel extends PurposeViewModel {
   private String redeliverUrl;

   public String getRedeliverUrl() {
      return redeliverUrl;
   }

   public void setRedeliverUrl(String redeliverUrl) {
      this.redeliverUrl = redeliverUrl;
   }

   @Override
   public String getPurpose() {
      return "deadLetter";
   }

   public DeadLetterQueueViewModel(String redeliverUrl) {
      super();
      this.redeliverUrl = redeliverUrl;
   }
}
