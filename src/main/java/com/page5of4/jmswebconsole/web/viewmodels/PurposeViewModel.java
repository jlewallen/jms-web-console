package com.page5of4.jmswebconsole.web.viewmodels;

public abstract class PurposeViewModel {
   public abstract String getPurpose();
}
