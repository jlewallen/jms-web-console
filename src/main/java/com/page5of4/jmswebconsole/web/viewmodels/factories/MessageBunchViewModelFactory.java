package com.page5of4.jmswebconsole.web.viewmodels.factories;

import java.util.Enumeration;
import java.util.List;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.page5of4.jmswebconsole.services.JmsUtils;
import com.page5of4.jmswebconsole.services.ServerProvider;
import com.page5of4.jmswebconsole.web.viewmodels.MessageBunchViewModel;
import com.page5of4.jmswebconsole.web.viewmodels.MessageViewModel;

@Service
public class MessageBunchViewModelFactory {
   private final ServerProvider serverProvider;

   @Autowired
   public MessageBunchViewModelFactory(ServerProvider serverProvider) {
      super();
      this.serverProvider = serverProvider;
   }

   public MessageBunchViewModel createMessageBunch(String address, String name, long number) {
      Connection connection = serverProvider.createConnection(address);
      try {
         List<MessageViewModel> messages = Lists.newArrayList();
         Session session = connection.createSession(true, Session.SESSION_TRANSACTED);
         Queue queue = session.createQueue(name);
         QueueBrowser browser = session.createBrowser(queue);
         Enumeration<?> enumeration = browser.getEnumeration();
         while(enumeration.hasMoreElements()) {
            if(number-- == 0) {
               break;
            }
            Message message = (Message)enumeration.nextElement();
            String id = message.getJMSMessageID();
            DateTime dateSent = new DateTime(message.getJMSTimestamp());
            String payload = "";
            if(message instanceof TextMessage) {
               payload = ((TextMessage)message).getText();
            }
            messages.add(new MessageViewModel(id, dateSent, payload));
         }
         JmsUtils.closeSession(session);
         return new MessageBunchViewModel(messages);
      }
      catch(JMSException e) {
         throw new RuntimeException(e);
      }
      finally {
         JmsUtils.closeConnection(connection);
      }
   }
}
