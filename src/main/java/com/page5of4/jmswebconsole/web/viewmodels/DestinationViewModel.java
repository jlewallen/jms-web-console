package com.page5of4.jmswebconsole.web.viewmodels;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.joda.time.DateTime;
import org.joda.time.Duration;

import com.google.common.collect.Lists;
import com.page5of4.jmswebconsole.domain.DestinationType;
import com.page5of4.jmswebconsole.web.Urls;

public class DestinationViewModel {
   private String name;
   private final String url;
   private final String deleteUrl;
   private final String pauseUrl;
   private final String purgeUrl;
   private final String messagesUrl;
   private DateTime oldestMessageTime;
   private String state;
   private long numberOfMessagesPendingAcks;
   private long totalMessagesBytes;
   private long numberOfActiveConsumers;
   private long numberOfConsumers;
   private long numberOfProducers;
   private long numberOfMessages;
   private long numberOfMessagesIn;
   private long numberOfMessagesOut;
   private DestinationType type;
   private final PurposeViewModel purpose;
   private final List<ConsumerViewModel> consumers = Lists.newArrayList();

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getUrl() {
      return url;
   }

   public String getDeleteUrl() {
      return deleteUrl;
   }

   public String getPauseUrl() {
      return pauseUrl;
   }

   public String getPurgeUrl() {
      return purgeUrl;
   }

   public String getMessagesUrl() {
      return messagesUrl;
   }

   public String getState() {
      return state;
   }

   public void setState(String state) {
      this.state = state;
   }

   @JsonSerialize(using = DateTimeSerializer.class)
   public DateTime getOldestMessageTime() {
      return oldestMessageTime;
   }

   public void setOldestMessageTime(DateTime oldestMessageTime) {
      this.oldestMessageTime = oldestMessageTime;
   }

   public Long getAgeOfOldestMessageInSeconds() {
      if(oldestMessageTime == null) return null;
      Duration duration = new Duration(oldestMessageTime, new DateTime());
      return duration.getStandardSeconds();
   }

   public DestinationType getType() {
      return type;
   }

   public void setType(DestinationType type) {
      this.type = type;
   }

   public long getNumberOfMessagesPendingAcks() {
      return numberOfMessagesPendingAcks;
   }

   public void setNumberOfMessagesPendingAcks(long numberOfMessagesPendingAcks) {
      this.numberOfMessagesPendingAcks = numberOfMessagesPendingAcks;
   }

   public long getTotalMessagesBytes() {
      return totalMessagesBytes;
   }

   public void setTotalMessagesBytes(long totalMessagesBytes) {
      this.totalMessagesBytes = totalMessagesBytes;
   }

   public long getNumberOfActiveConsumers() {
      return numberOfActiveConsumers;
   }

   public void setNumberOfActiveConsumers(long numberOfActiveConsumers) {
      this.numberOfActiveConsumers = numberOfActiveConsumers;
   }

   public long getNumberOfConsumers() {
      return numberOfConsumers;
   }

   public void setNumberOfConsumers(long numberOfConsumers) {
      this.numberOfConsumers = numberOfConsumers;
   }

   public long getNumberOfProducers() {
      return numberOfProducers;
   }

   public void setNumberOfProducers(long numberOfProducers) {
      this.numberOfProducers = numberOfProducers;
   }

   public long getNumberOfMessages() {
      return numberOfMessages;
   }

   public void setNumberOfMessages(long numberOfMessages) {
      this.numberOfMessages = numberOfMessages;
   }

   public long getNumberOfMessagesIn() {
      return numberOfMessagesIn;
   }

   public void setNumberOfMessagesIn(long numberOfMessagesIn) {
      this.numberOfMessagesIn = numberOfMessagesIn;
   }

   public long getNumberOfMessagesOut() {
      return numberOfMessagesOut;
   }

   public void setNumberOfMessagesOut(long numberOfMessagesOut) {
      this.numberOfMessagesOut = numberOfMessagesOut;
   }

   public List<ConsumerViewModel> getConsumers() {
      return consumers;
   }

   public PurposeViewModel getPurpose() {
      return purpose;
   }

   public DestinationViewModel(String server, String name, PurposeViewModel purpose) {
      super();
      this.url = Urls.makeDestinationUrl(server, name);
      this.deleteUrl = Urls.makeDeleteDestinationUrl(server, name);
      this.pauseUrl = Urls.makePauseDestinationUrl(server, name);
      this.purgeUrl = Urls.makePurgeDestinationUrl(server, name);
      this.messagesUrl = Urls.makeDestinationMessagesUrl(server, name);
      this.name = name;
      this.purpose = purpose;
   }
}
