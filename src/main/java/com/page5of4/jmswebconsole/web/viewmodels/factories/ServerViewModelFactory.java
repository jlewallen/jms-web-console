package com.page5of4.jmswebconsole.web.viewmodels.factories;

import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Maps;
import com.page5of4.jmswebconsole.domain.Consumer;
import com.page5of4.jmswebconsole.domain.Destination;
import com.page5of4.jmswebconsole.domain.Server;
import com.page5of4.jmswebconsole.domain.repositories.ServerRepository;
import com.page5of4.jmswebconsole.services.QueueAddress;
import com.page5of4.jmswebconsole.web.Urls;
import com.page5of4.jmswebconsole.web.viewmodels.ConsumerViewModel;
import com.page5of4.jmswebconsole.web.viewmodels.DeadLetterQueueViewModel;
import com.page5of4.jmswebconsole.web.viewmodels.DestinationViewModel;
import com.page5of4.jmswebconsole.web.viewmodels.PurposeViewModel;
import com.page5of4.jmswebconsole.web.viewmodels.ServerViewModel;

@Service
public class ServerViewModelFactory {
   private final ServerRepository serverRepository;

   @Autowired
   public ServerViewModelFactory(ServerRepository serverRepository) {
      super();
      this.serverRepository = serverRepository;
   }

   public static class DestinationFilter implements Predicate<Destination> {
      public static DestinationFilter INSTANCE = new DestinationFilter();

      @Override
      public boolean apply(Destination d) {
         return !d.getName().startsWith("mq.");
      }
   }

   public ServerViewModel createServer(String address) {
      Server server = serverRepository.findServer(address);
      ServerViewModel serverViewModel = new ServerViewModel(server.getAddress(), server.getName());
      Map<String, DestinationViewModel> destinations = Maps.newHashMap();
      for(final Destination destination : Collections2.filter(server.getDestinations(), DestinationFilter.INSTANCE)) {
         DestinationViewModel destinationViewModel = new DestinationViewModel(address, destination.getName(), createPurposeViewModel(address, destination)) {
            {
               setNumberOfActiveConsumers(destination.getNumberOfActiveConsumers());
               setNumberOfConsumers(destination.getNumberOfConsumers());
               setNumberOfProducers(destination.getNumberOfProducers());
               setNumberOfMessages(destination.getNumberOfMessages());
               setNumberOfMessagesIn(destination.getNumberOfMessagesIn());
               setNumberOfMessagesOut(destination.getNumberOfMessagesOut());
               setNumberOfMessagesPendingAcks(destination.getNumberOfMessagesPendingAcks());
               setOldestMessageTime(destination.getOldestMessageTime());
               setState(destination.getState());
               setTotalMessagesBytes(destination.getTotalMessagesBytes());
               setType(destination.getType());
            }
         };
         destinations.put(destinationViewModel.getName(), destinationViewModel);
         serverViewModel.getDestinations().add(destinationViewModel);
      }
      for(final Consumer consumer : server.getConsumers()) {
         ConsumerViewModel consumerViewModel = new ConsumerViewModel() {
            {
               setConsumerId(consumer.getConsumerId());
               setDestinationName(consumer.getDestinationName());
               setDestinationType(consumer.getDestinationType());
               setLastAcknowledgeTime(consumer.getLastAcknowledgeTime());
               setNumberOfMessages(consumer.getNumberOfMessages());
               setNumberOfMessagesPendingAcks(consumer.getNumberOfMessagesPendingAcks());
               setServiceName(consumer.getServiceName());
               setHost(consumer.getHost());
               setUser(consumer.getUser());
               setPaused(consumer.isPaused());
            }
         };
         if(destinations.containsKey(consumer.getDestinationName())) {
            destinations.get(consumer.getDestinationName()).getConsumers().add(consumerViewModel);
         }
         serverViewModel.getConsumers().add(consumerViewModel);
      }
      Collections.sort(serverViewModel.getDestinations(), new Comparator<DestinationViewModel>() {
         @Override
         public int compare(DestinationViewModel o1, DestinationViewModel o2) {
            return o1.getName().compareTo(o2.getName());
         }
      });
      Collections.sort(serverViewModel.getConsumers(), new Comparator<ConsumerViewModel>() {
         @Override
         public int compare(ConsumerViewModel o1, ConsumerViewModel o2) {
            return o1.getDestinationName().compareTo(o2.getDestinationName());
         }
      });
      return serverViewModel;
   }

   private static final Pattern DEAD_LETTER_PATTERN = Pattern.compile("(.+)\\.deadLetter");

   private PurposeViewModel createPurposeViewModel(String address, Destination destination) {
      Matcher matcher = DEAD_LETTER_PATTERN.matcher(destination.getName());
      if(matcher.matches()) {
         String actualQueueName = matcher.group(1);
         String redeliverUrl = Urls.makeRedeliverUrl(new QueueAddress(address, destination.getName()), new QueueAddress(address, actualQueueName));
         return new DeadLetterQueueViewModel(redeliverUrl);
      }
      return null;
   }

   public DestinationViewModel createDestinationViewModel(String address, String name) {
      ServerViewModel server = createServer(address);
      for(DestinationViewModel destinationViewModel : server.getDestinations()) {
         if(destinationViewModel.getName().equals(name)) {
            return destinationViewModel;
         }
      }
      throw new RuntimeException("No such Destiantion: " + name);
   }
}
