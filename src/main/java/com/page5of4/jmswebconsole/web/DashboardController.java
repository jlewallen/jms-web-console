package com.page5of4.jmswebconsole.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.page5of4.mustache.SingleModelAndView;

@Controller
@RequestMapping(value = "/")
public class DashboardController {
   @RequestMapping
   public ModelAndView index() {
      return new SingleModelAndView("dashboard/dashboard", new DashboardViewModel(Urls.makeServersUrl()));
   }

   public static class DashboardViewModel {
      private final String serversUrl;

      public String getServersUrl() {
         return serversUrl;
      }

      public DashboardViewModel(String serversUrl) {
         super();
         this.serversUrl = serversUrl;
      }
   }
}
