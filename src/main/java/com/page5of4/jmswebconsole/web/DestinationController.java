package com.page5of4.jmswebconsole.web;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.page5of4.jmswebconsole.domain.PauseType;
import com.page5of4.jmswebconsole.services.DeadLetterRedelivery;
import com.page5of4.jmswebconsole.services.DestinationService;
import com.page5of4.jmswebconsole.services.QueueAddress;
import com.page5of4.jmswebconsole.web.viewmodels.factories.MessageBunchViewModelFactory;
import com.page5of4.jmswebconsole.web.viewmodels.factories.ServerViewModelFactory;
import com.page5of4.mustache.SingleModelAndView;

@Controller
@RequestMapping(value = "destinations")
public class DestinationController {
   private final ServerViewModelFactory serverViewModelFactory;
   private final MessageBunchViewModelFactory messageBunchViewModelFactory;
   private final DestinationService destinationService;
   private final DeadLetterRedelivery deadLetterRedelivery;

   @Autowired
   public DestinationController(ServerViewModelFactory serverViewModelFactory, MessageBunchViewModelFactory messageBunchViewModelFactory, DestinationService destinationService, DeadLetterRedelivery deadLetterRedelivery) {
      super();
      this.serverViewModelFactory = serverViewModelFactory;
      this.messageBunchViewModelFactory = messageBunchViewModelFactory;
      this.destinationService = destinationService;
      this.deadLetterRedelivery = deadLetterRedelivery;
   }

   @RequestMapping(params = { "server", "name" })
   public ModelAndView destination(@RequestParam String server, @RequestParam String name) {
      return new SingleModelAndView("empty", serverViewModelFactory.createDestinationViewModel(server, name));
   }

   @RequestMapping(value = "messages", params = { "server", "name" })
   public ModelAndView messages(@RequestParam String server, @RequestParam String name, @RequestParam(defaultValue = "25") long number) {
      return new SingleModelAndView("empty", messageBunchViewModelFactory.createMessageBunch(server, name, number));
   }

   @RequestMapping(value = "pause", params = { "server", "names", "type" }, method = RequestMethod.POST)
   public ModelAndView pause(@RequestParam String server, @RequestParam String[] names, @RequestParam PauseType type) {
      return new SingleModelAndView("empty", destinationService.pause(server, names, type));
   }

   @RequestMapping(value = "purge", params = { "server", "names" }, method = RequestMethod.POST)
   public ModelAndView purge(@RequestParam String server, @RequestParam String[] names) {
      return new SingleModelAndView("empty", destinationService.purge(server, names));
   }

   @RequestMapping(value = "delete", params = { "server", "names" }, method = RequestMethod.POST)
   public ModelAndView delete(@RequestParam String server, @RequestParam String[] names) {
      return new SingleModelAndView("empty", destinationService.delete(server, names));
   }

   @RequestMapping(value = "redeliver", params = { "from", "to" }, method = RequestMethod.POST)
   public ModelAndView redeliver(@RequestParam QueueAddress from, @RequestParam QueueAddress to, @RequestParam(defaultValue = "1") long number) {
      deadLetterRedelivery.redeliver(from, to, number);
      return new SingleModelAndView("empty", serverViewModelFactory.createServer(to.getServer()));
   }

   @InitBinder
   public void initializeBinder(WebDataBinder binder) {
      binder.registerCustomEditor(QueueAddress.class, new PropertyEditorSupport() {
         @Override
         public void setAsText(String text) throws IllegalArgumentException {
            setValue(QueueAddress.parse(text));
         }
      });
   }
}
