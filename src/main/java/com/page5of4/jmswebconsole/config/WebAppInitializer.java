package com.page5of4.jmswebconsole.config;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.DispatcherServlet;

public class WebAppInitializer implements WebApplicationInitializer {
   private static final Logger logger = LoggerFactory.getLogger(WebAppInitializer.class);

   private String getActiveProfile() {
      String profile = System.getProperty("spring.active.profiles");
      if(StringUtils.isEmpty(profile)) {
         return "production";
      }
      return profile;
   }

   @Override
   public void onStartup(ServletContext container) {
      AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
      ConfigurableEnvironment env = rootContext.getEnvironment();
      env.setActiveProfiles(getActiveProfile());
      for(String profile : env.getActiveProfiles()) {
         logger.info("Profile: {}", profile);
      }
      rootContext.register(SpringConfig.class);

      ContextLoader contextLoader = new ContextLoader(rootContext);
      contextLoader.initWebApplicationContext(container);

      AnnotationConfigWebApplicationContext dispatcherContext = new AnnotationConfigWebApplicationContext();
      dispatcherContext.setParent(rootContext);
      dispatcherContext.register(WebAppConfig.class);

      ServletRegistration.Dynamic dispatcher = container.addServlet("dispatcher", new DispatcherServlet(dispatcherContext));
      dispatcher.setLoadOnStartup(1);
      dispatcher.addMapping("/*");
      // addOpenEntityManagerInViewFilter(container);
      addHttpMethodFilter(container);
   }

   private void addHttpMethodFilter(ServletContext context) {
      FilterRegistration.Dynamic method = context.addFilter("HttpMethodFilter", HiddenHttpMethodFilter.class);
      method.addMappingForUrlPatterns(null, true, "/*");
   }

   private void addOpenEntityManagerInViewFilter(ServletContext context) {
      FilterRegistration.Dynamic entity = context.addFilter("SpringOpenEntityManagerInViewFilter", OpenEntityManagerInViewFilter.class);
      entity.addMappingForUrlPatterns(null, true, "/*");
   }
}
