package com.page5of4.jmswebconsole.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.page5of4.jmswebconsole.services.ServerProvider;
import com.page5of4.jmswebconsole.services.TestQueuesCreator;

@Profile("dev")
@Configuration
public class DevConfig {
   @Autowired
   private ServerProvider provider;

   @Bean
   public TestQueuesCreator testQueuesCreator() {
      return new TestQueuesCreator(provider);
   }
}
