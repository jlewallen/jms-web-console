package com.page5of4.jmswebconsole.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ExcludeFromScan
@Import(DefaultSpringMvcConfig.class)
@ComponentScan(basePackages = "com.page5of4.jmswebconsole.web", excludeFilters = @Filter(ExcludeFromScan.class))
public class WebAppConfig {

}
