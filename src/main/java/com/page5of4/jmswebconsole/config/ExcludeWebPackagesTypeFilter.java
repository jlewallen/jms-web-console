package com.page5of4.jmswebconsole.config;

import java.util.regex.Pattern;

import org.springframework.core.type.filter.RegexPatternTypeFilter;

public class ExcludeWebPackagesTypeFilter extends RegexPatternTypeFilter {
   public ExcludeWebPackagesTypeFilter() {
      super(Pattern.compile(".*\\.web\\.*"));
   }
}
