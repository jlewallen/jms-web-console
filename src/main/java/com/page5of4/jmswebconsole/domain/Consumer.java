package com.page5of4.jmswebconsole.domain;

import java.util.Date;

public class Consumer {
   private String consumerId;
   private String host;
   private String user;
   private String serviceName;
   private String destinationName;
   private DestinationType destinationType;
   private Date lastAcknowledgeTime;
   private long numberOfMessages;
   private long numberOfMessagesPendingAcks;
   private boolean paused;

   public String getConsumerId() {
      return consumerId;
   }

   public void setConsumerId(String consumerId) {
      this.consumerId = consumerId;
   }

   public String getHost() {
      return host;
   }

   public void setHost(String host) {
      this.host = host;
   }

   public String getUser() {
      return user;
   }

   public void setUser(String user) {
      this.user = user;
   }

   public String getServiceName() {
      return serviceName;
   }

   public void setServiceName(String serviceName) {
      this.serviceName = serviceName;
   }

   public String getDestinationName() {
      return destinationName;
   }

   public void setDestinationName(String destinationName) {
      this.destinationName = destinationName;
   }

   public DestinationType getDestinationType() {
      return destinationType;
   }

   public void setDestinationType(DestinationType destinationType) {
      this.destinationType = destinationType;
   }

   public Date getLastAcknowledgeTime() {
      return lastAcknowledgeTime;
   }

   public void setLastAcknowledgeTime(Date lastAcknowledgeTime) {
      this.lastAcknowledgeTime = lastAcknowledgeTime;
   }

   public long getNumberOfMessages() {
      return numberOfMessages;
   }

   public void setNumberOfMessages(long numberOfMessages) {
      this.numberOfMessages = numberOfMessages;
   }

   public long getNumberOfMessagesPendingAcks() {
      return numberOfMessagesPendingAcks;
   }

   public void setNumberOfMessagesPendingAcks(long numberOfMessagesPendingAcks) {
      this.numberOfMessagesPendingAcks = numberOfMessagesPendingAcks;
   }

   public boolean isPaused() {
      return paused;
   }

   public void setPaused(boolean paused) {
      this.paused = paused;
   }
}
