package com.page5of4.jmswebconsole.domain;

public enum PauseType {
   ALL,
   CONSUMERS,
   PRODUCERS,
   NONE
}
