package com.page5of4.jmswebconsole.domain;

import java.util.List;

import com.google.common.collect.Lists;

public class Server {
   private String address;
   private String name;
   private final List<Destination> destinations = Lists.newArrayList();
   private final List<Consumer> consumers = Lists.newArrayList();

   public String getAddress() {
      return address;
   }

   public void setAddress(String address) {
      this.address = address;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Server(String address, String name) {
      super();
      this.address = address;
      this.name = name;
   }

   public List<Destination> getDestinations() {
      return destinations;
   }

   public List<Consumer> getConsumers() {
      return consumers;
   }
}
