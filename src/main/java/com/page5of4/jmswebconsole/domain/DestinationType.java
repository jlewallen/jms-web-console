package com.page5of4.jmswebconsole.domain;

public enum DestinationType {
   Queue,
   Topic
}
