package com.page5of4.jmswebconsole.domain.repositories;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.page5of4.jmswebconsole.domain.Server;
import com.page5of4.jmswebconsole.services.ServerProvider;

@Service
public class ServerRepository {
   private final ServerProvider serverProvider;

   @Value("${servers:127.0.0.1:7676}")
   private String[] servers;

   @Autowired
   public ServerRepository(ServerProvider serverProvider) {
      super();
      this.serverProvider = serverProvider;
   }

   public Collection<Server> findAll() {
      Collection<Server> servers = Lists.newArrayList();
      for(String address : this.servers) {
         servers.add(findServer(address));
      }
      return servers;
   }

   public Server findServer(String address) {
      return serverProvider.createServer(address);
   }
}
