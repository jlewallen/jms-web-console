package com.page5of4.jmswebconsole.domain;

import org.joda.time.DateTime;

public class Destination {
   private String name;
   private String state;
   private DestinationType type;
   private DateTime oldestMessageTime;
   private long numberOfMessagesPendingAcks;
   private long totalMessagesBytes;
   private long numberOfActiveConsumers;
   private long numberOfConsumers;
   private long numberOfActiveProducers;
   private long numberOfProducers;
   private long numberOfMessages;
   private long numberOfMessagesIn;
   private long numberOfMessagesOut;

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getState() {
      return state;
   }

   public void setState(String state) {
      this.state = state;
   }

   public DestinationType getType() {
      return type;
   }

   public DateTime getOldestMessageTime() {
      return oldestMessageTime;
   }

   public void setOldestMessageTime(DateTime oldestMessageTime) {
      this.oldestMessageTime = oldestMessageTime;
   }

   public void setType(DestinationType type) {
      this.type = type;
   }

   public long getNumberOfMessagesPendingAcks() {
      return numberOfMessagesPendingAcks;
   }

   public void setNumberOfMessagesPendingAcks(long numberOfMessagesPendingAcks) {
      this.numberOfMessagesPendingAcks = numberOfMessagesPendingAcks;
   }

   public long getTotalMessagesBytes() {
      return totalMessagesBytes;
   }

   public void setTotalMessagesBytes(long totalMessagesBytes) {
      this.totalMessagesBytes = totalMessagesBytes;
   }

   public long getNumberOfActiveConsumers() {
      return numberOfActiveConsumers;
   }

   public void setNumberOfActiveConsumers(long numberOfActiveConsumers) {
      this.numberOfActiveConsumers = numberOfActiveConsumers;
   }

   public long getNumberOfConsumers() {
      return numberOfConsumers;
   }

   public void setNumberOfConsumers(long numberOfConsumers) {
      this.numberOfConsumers = numberOfConsumers;
   }

   public long getNumberOfActiveProducers() {
      return numberOfActiveProducers;
   }

   public void setNumberOfActiveProducers(long numberOfActiveProducers) {
      this.numberOfActiveProducers = numberOfActiveProducers;
   }

   public long getNumberOfProducers() {
      return numberOfProducers;
   }

   public void setNumberOfProducers(long numberOfProducers) {
      this.numberOfProducers = numberOfProducers;
   }

   public long getNumberOfMessages() {
      return numberOfMessages;
   }

   public void setNumberOfMessages(long numberOfMessages) {
      this.numberOfMessages = numberOfMessages;
   }

   public long getNumberOfMessagesIn() {
      return numberOfMessagesIn;
   }

   public void setNumberOfMessagesIn(long numberOfMessagesIn) {
      this.numberOfMessagesIn = numberOfMessagesIn;
   }

   public long getNumberOfMessagesOut() {
      return numberOfMessagesOut;
   }

   public void setNumberOfMessagesOut(long numberOfMessagesOut) {
      this.numberOfMessagesOut = numberOfMessagesOut;
   }

   public Destination(String name) {
      super();
      this.name = name;
   }
}
