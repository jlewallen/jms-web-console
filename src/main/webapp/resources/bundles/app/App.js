Ext.define('WebConsole.App', {
   extend : 'Ext.container.Viewport',
   requires : [ 'WebConsole.ServersPanel', 'WebConsole.Tabs', 'WebConsole.ServerDashboard', 'WebConsole.MessagesPanel', 'WebConsole.DestinationsGrid', 'WebConsole.DestinationPanel',
         'WebConsole.PurposePanel' ],

   initComponent : function() {
      Ext.define('Server', {
         extend : 'Ext.data.Model',
         idProperty : 'address',
         fields : [ 'address', 'url', 'name', 'purgeUrl', 'pauseUrl', 'deleteUrl' ],
         hasMany : 'Destination',
         proxy : {
            type : 'rest',
            reader : 'json'
         },
      });

      Ext.define('Destination', {
         extend : 'Ext.data.Model',
         idProperty : 'name',
         fields : [ 'name', 'url', 'type', 'state', "numberOfMessagesPendingAcks", "totalMessagesBytes", "numberOfActiveConsumers", "numberOfConsumers", "numberOfMessages", "numberOfMessagesIn",
               "numberOfMessagesOut", "purgeUrl", "pauseUrl", "deleteUrl", "messagesUrl", "purpose" ],
         hasMany : 'Consumer'
      });

      Ext.define('DeadLetterQueue', {
         extend : 'Ext.data.Model',
         fields : [ 'redeliverUrl', 'purpose' ]
      });

      Ext.define('Message', {
         extend : 'Ext.data.Model',
         fields : [ 'id', 'payload', 'dateSent', 'ageInSeconds' ]
      });

      Ext.define('Consumer', {
         extend : 'Ext.data.Model',
         fields : [ 'host', 'paused' ]
      });

      Ext.apply(this, {
         layout : 'border',
         padding : 5,
         items : [ this.createServerPanel(), this.createTabs() ]
      });
      this.callParent(arguments);
   },

   createServerPanel : function() {
      this.serversPanel = Ext.create('widget.serverspanel', {
         region : 'west',
         collapsible : true,
         width : 225,
         floatable : false,
         split : true,
         url : this.url,
         minWidth : 175,
         listeners : {
            scope : this,
            serverSelect : this.onServerSelect
         }
      });
      return this.serversPanel;
   },

   createTabs : function() {
      this.tabs = Ext.create('widget.tabs', {
         region : 'center',
         minWidth : 300,
         listeners : {
            scope : this,
            openMessages : this.onOpenMessages
         }
      });
      return this.tabs;
   },

   onServerSelect : function(source, server) {
      this.tabs.showServer(server.get("name"), server.get("url"));
   },

   onOpenMessages : function(source, server) {
      console.log(server);
   }
});
