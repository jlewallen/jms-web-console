Ext.define('WebConsole.DestinationsGrid', {
   extend : 'Ext.grid.Panel',
   alias : 'widget.destinationsgrid',

   initComponent : function() {
      this.addEvents('rowdblclick', 'select');

      Ext.apply(this, {
         cls : 'destinations-grid',
         store : {
            model : 'Destination'
         },
         viewConfig : {
            itemId : 'view',
            plugins : [ {
               pluginId : 'preview',
               ptype : 'preview',
               bodyField : 'type',
               previewExpanded : false
            } ],
            listeners : {
               scope : this,
               itemdblclick : this.onRowDblClick
            },
            getRowClass : function(record, rowIdx, params, store) {
               if (record.get("numberOfMessagesPendingAcks") > 0) {
                  return 'queue pending-messages';
               }
               else if (record.get("numberOfMessages") > 0) {
                  return 'queue messages';
               }
               else {
                  return 'queue';
               }
            }
         },
         selModel : {
            mode : 'MULTI'
         },
         columns : [ {
            text : 'Name',
            dataIndex : 'name',
            flex : 1,
            renderer : this.makeDisplayName
         }, {
            text : 'Oldest Message',
            dataIndex : 'ageOfOldestMessageInSeconds',
            flex : 0,
            width : 120,
            renderer : function(value, p, record) {
               if (value == null) return "";
               return jQuery.timeago(value * 1000);
            }
         }, {
            text : 'M',
            dataIndex : 'numberOfMessages',
            flex : 0,
            width : 40
         }, {
            text : 'P',
            dataIndex : 'numberOfMessagesPendingAcks',
            flex : 0,
            width : 40
         }, {
            text : 'I',
            dataIndex : 'numberOfMessagesIn',
            flex : 0,
            width : 40
         }, {
            text : 'O',
            dataIndex : 'numberOfMessagesOut',
            flex : 0,
            width : 40
         }, {
            text : 'C',
            dataIndex : 'numberOfConsumers',
            flex : 0,
            width : 40
         } ]
      });
      this.callParent(arguments);
      this.addEvents('destinationSelect');
      this.on('selectionchange', this.onSelect, this);
   },

   onRowDblClick : function(view, record, item, index, e) {
      this.fireEvent('rowdblclick', this, this.store.getAt(index));
   },

   onSelect : function(model, selections) {
      var selected = selections[0];
      if (selected) {
         this.fireEvent('destinationSelect', this, selected);
      }
   },

   getSelected : function() {
      return this.getSelectionModel().getSelection();
   },

   onLoad : function(store, records) {
      this.getSelectionModel().select(0);
   },

   makeDisplayName : function(value, p, record) {
      return Ext.String.format('<div class="destination"><b>{0}</b><span class="type-state">{1}</span></div>', value,
            record.get('state'));
   },

});
