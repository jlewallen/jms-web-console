Ext.define('WebConsole.ServerDashboard', {
   extend : 'Ext.panel.Panel',
   alias : 'widget.serverdashboard',
   border : false,

   initComponent : function() {
      Ext.apply(this, {
         layout : 'border',
         items : this.createDestinationsGrid()
      });
      this.periodicRefresh = true;
      this.loadServer(this.url);
      this.callParent(arguments);
   },

   loadServerModelFromJson : function(data) {
      var self = this;
      var sm = self.destinationsGrid.getSelectionModel();
      self.server = data;
      var restoreSelection = function() {};
      if (sm.hasSelection()) {
         var selection = sm.getSelection();
         for ( var i = 0; i < selection.length; i++) {
            selection[i] = selection[i].getId();
         }
         restoreSelection = function() {
            for ( var i = 0; i < selection.length; i++) {
               sm.select(self.destinationsGrid.getStore().getById(selection[i]), true);
            }
         };
      }
      self.destinationsGrid.getStore().loadData(data.destinations);
      self.destinationsGrid.getView().refresh();
      restoreSelection();
   },

   loadServer : function(url) {
      var self = this;
      Ext.Ajax.request({
         url : url,
         method : 'GET',
         params : {
            "_format" : "json"
         },
         success : function(response) {
            var data = Ext.decode(response.responseText);
            self.loadServerModelFromJson(data);
         },
         failure : function() {

         }
      });
      self.url = url;
      self.onAutoRefreshToggle();
   },

   createDestinationsGrid : function() {
      this.destinationsGrid = Ext.create('widget.destinationsgrid', {
         region : 'center',
         dockedItems : [ this.createTopToolbar() ],
         flex : 2,
         minHeight : 700,
         minWidth : 150,
         listeners : {
            scope : this
         }
      });
      return this.destinationsGrid;
   },

   createPurposePanel : function(destination) {
      var self = this;
      return self.purposePanel = Ext.create('widget.purposepanel', {
         region : 'south',
         flex : 2,
         height : 140,
         destination : destination,
         listeners : {
            scope : this,
            refreshServer : function(panel, data) {
               self.loadServerModelFromJson(data);
            }
         }
      });
   },

   createTopToolbar : function() {
      this.toolbar = Ext.create('widget.toolbar', {
         cls : 'x-docked-noborder-top',
         items : [ {
            iconCls : 'refresh',
            text : 'Refresh',
            scope : this,
            handler : this.onRefreshClick
         }, {
            iconCls : 'auto-refresh',
            text : 'Auto Refreshing',
            enableToggle : true,
            pressed : true,
            scope : this,
            toggleHandler : this.onAutoRefreshToggle
         }, "-", {
            iconCls : 'summary',
            text : 'Summary',
            enableToggle : true,
            pressed : false,
            scope : this,
            toggleHandler : this.onSummaryToggle
         }, "-", {
            iconCls : 'summary',
            text : 'Messages',
            scope : this,
            handler : this.onMessagesClick
         }, "-", {
            iconCls : 'delete',
            text : 'Delete',
            scope : this,
            handler : this.onDeleteClick
         }, {
            iconCls : 'purge',
            text : 'Purge',
            scope : this,
            handler : this.onPurgeClick
         }, {
            iconCls : 'pause',
            text : 'Pause',
            scope : this,
            handler : this.onPauseClick
         }, "-", {
            iconCls : 'redeliver',
            text : 'Redeliver',
            scope : this,
            handler : this.onRedeliverClick
         } ]
      });
      return this.toolbar;
   },

   onMessagesClick : function() {
      var self = this;
      Ext.each(this.destinationsGrid.getSelected(), function(d) {
         self.fireEvent("openmessages", self, d);
      });
   },

   onOpenAllClick : function() {

   },

   onRefreshClick : function() {
      this.loadServer(this.url);
   },

   onAutoRefreshToggle : function(btn, pressed) {
      var self = this;
      if (pressed != null) {
         self.periodicRefresh = pressed;
      }
      if (!self.periodicRefresh) {
         if (self.refreshTimer != null) {
            clearInterval(self.refreshTimer);
            self.refreshTimer = null;
         }
      }
      else {
         if (this.refreshTimer == null) {
            self.refreshTimer = setInterval(function() {
               self.loadServer(self.url);
            }, 5000);
         }
      }
   },

   onSummaryToggle : function(btn, pressed) {
      this.destinationsGrid.getComponent('view').getPlugin('preview').toggleExpanded(pressed);
   },

   ajax : function(url, names) {
      var self = this;
      Ext.Ajax.request({
         url : url,
         params : {
            names : names
         },
         method : 'POST',
         success : function(response) {
            var data = Ext.decode(response.responseText);
            self.loadServerModelFromJson(data);
         }
      });
   },

   onPurgeClick : function() {
      var self = this;
      var names = [];
      Ext.each(this.destinationsGrid.getSelected(), function(d) {
         names.push(d.get("name"));
      });
      self.ajax(self.server.purgeUrl, names);
   },

   onDeleteClick : function() {
      var self = this;
      var names = [];
      Ext.each(this.destinationsGrid.getSelected(), function(d) {
         names.push(d.get("name"));
      });
      self.ajax(self.server.deleteUrl, names);
   },

   onRedeliverClick : function() {
      var self = this;
      Ext.each(this.destinationsGrid.getSelected(), function(d) {
         if (d.get("purpose")) {
            var dialog = new Ext.create("Ext.Window", {
               title : d.get("name"),
               modal : true,
               width : 500,
               height : 250,
               shadow : true,
               proxyDrag : true,
               border : false,
               items : self.createPurposePanel(d)
            });
            dialog.show();
         }
      });
   },

   onPauseClick : function() {
      var self = this;
      var typesToNames = {};
      Ext.each(this.destinationsGrid.getSelected(), function(d) {
         var type = 'NONE';
         if (d.get("state") == 'Running') {
            type = 'ALL';
         }
         else if (d.get("state") == 'Paused') {
            type = 'CONSUMERS';
         }
         typesToNames[type] = typesToNames[type] || [];
         typesToNames[type].push(d.get("name"));
      });
      Ext.iterate(typesToNames, function(k, v) {
         Ext.Ajax.request({
            url : self.server.pauseUrl + "&type=" + k,
            method : 'POST',
            params : {
               '_format' : 'json',
               names : v
            },
            success : function(response) {
               var data = Ext.decode(response.responseText);
               self.loadServerModelFromJson(data);
            }
         });
      });
   }
});
