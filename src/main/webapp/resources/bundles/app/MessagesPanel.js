Ext.define('WebConsole.MessagesPanel', {
   extend : 'Ext.panel.Panel',
   alias : 'widget.messagespanel',
   border : false,

   initComponent : function() {
      Ext.apply(this, {
         layout : 'border',
         items : this.createGrid()
      });
      this.callParent(arguments);
   },

   createGrid : function() {
      var self = this;
      var store = new Ext.data.Store({
         model : 'Message',
         fields : [ 'id', 'ageInSeconds', 'dateSent', 'payload' ],
         proxy : {
            type : 'ajax',
            url : self.url,
            extraParams : {
               "_format" : "json"
            },
            reader : {
               type : 'json',
               root : 'messages'
            }
         },
         autoLoad : true,
         listeners : {
            load : function() {},
            scope : this
         }
      });
      return this.grid = Ext.create('Ext.grid.Panel', {
         region : 'center',
         listeners : {
            scope : this
         },
         cls : 'messages-grid',
         store : store,
         viewConfig : {
            itemId : 'view',
            plugins : [ {
               pluginId : 'preview',
               ptype : 'preview',
               bodyField : 'payload',
               previewExpanded : true,
               renderer : function(value) {
                  var escapeHTML = (function() {
                     var MAP = {
                        '&' : '&amp;',
                        '<' : '&lt;',
                        '>' : '&gt;',
                        '"' : '&#34;',
                        "'" : '&#39;'
                     };
                     var repl = function(c) {
                        return MAP[c];
                     };
                     return function(s) {
                        return s.replace(/[&<>'"]/g, repl);
                     };
                  })()
                  return "<pre>" + escapeHTML(value) + "</pre>";
               }
            } ],
            listeners : {
               scope : this
            }
         },
         selModel : {
            mode : 'SINGLE'
         },
         columns : [ {
            text : 'Message',
            dataIndex : 'id',
            flex : 1,
            renderer : function(value) {
               return "<tt>" + value + "</tt>";
            }
         }, {
            text : 'Age',
            dataIndex : 'ageInSeconds',
            width : 125,
            flex : 0,
            renderer : function(value, p, record) {
               return jQuery.timeago(value * 1000);
            }
         }, {
            text : 'Sent',
            dataIndex : 'dateSent',
            width : 150,
            flex : 0
         } ]
      });
   }
});
