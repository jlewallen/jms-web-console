Ext.define('WebConsole.PurposePanel', {
   extend : 'Ext.panel.Panel',
   alias : 'widget.purposepanel',

   initComponent : function() {
      Ext.apply(this, {
         border : false,
         frame : true,
         layout : 'fit',
         items : this.createView()
      });
      this.callParent(arguments);
   },

   createView : function() {
      var self = this;
      this.view = Ext.create('Ext.form.FormPanel', {
         frame : true,
         layout : 'fit',
         title : 'Dead Letter Redelivery',
         bodyStyle : 'padding: 5px 5px 0',
         border : false,
         defaultType : 'textfield',
         items : [/*
                   * { fieldLabel : 'From', xtype : 'textfield', name : 'from', value : 'df', readonly : true },
                   */{
            fieldLabel : 'Number',
            name : 'number',
            xtype : 'numberfield',
            value : 1,
            minValue : 0,
            maxValue : 100,
            allowBlank : false
         } ],

         buttons : [ {
            text : 'Redeliver',
            scope : this,
            handler : function() {
               var purpose = self.destination.get("purpose");
               this.view.getForm().submit({
                  url : purpose.redeliverUrl,
                  waitTitle : 'Connecting',
                  waitMsg : 'Sending data...',
                  // We don't return success: true...
                  failure : function(form, action) {
                     self.fireEvent('refreshServer', this, action.result);
                  }
               });
            }
         } ]
      });
      return this.view;
   },

   setActive : function(destination) {
      this.destination = destination;
   },

   onDestroy : function() {}
});
