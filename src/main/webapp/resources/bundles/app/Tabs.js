Ext.define('WebConsole.Tabs', {
   extend : 'Ext.tab.Panel',
   alias : 'widget.tabs',
   maxTabWidth : 230,
   border : false,

   initComponent : function() {
      this.tabBar = {
         border : true
      };

      this.callParent();
   },

   showServer : function(name, url) {
      var active = this.items.first();
      if (!active) {
         active = this.add({
            xtype : 'serverdashboard',
            title : name,
            url : url,
            closable : false,
            listeners : {
               scope : this,
               openall : this.onOpenAll,
               openmessages : this.onOpenMessages
            }
         });
      }
      else {
         active.loadServer(url);
         active.tab.setText(name);
      }
      this.setActiveTab(active);
   },

   onOpenMessages : function(source, destination) {
      if (destination) {
         var active = this.add({
            xtype : 'messagespanel',
            title : destination.get("name"),
            closable : true,
            url : destination.get("messagesUrl"),
            listeners : {
               scope : this
            }
         });
         this.setActiveTab(active);
      }
   },

   onOpenAll : function(detail) {

   }
});
