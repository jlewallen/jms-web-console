Ext.define('WebConsole.DestinationPanel', {
   extend : 'Ext.panel.Panel',

   alias : 'widget.destinationpanel',

   initComponent : function() {
      Ext.apply(this, {
         dockedItems : [ this.createToolbar() ],
         tpl : Ext.create('Ext.XTemplate', '<div class="destination">', '<h2>{name}</h2>', '<table>',
               '<tr><th>State</th><td>{state}</td></tr>', '<tr><th>Tye</th><td>{type}</td></tr>',
               '<tr><th>Number of Messages</th><td>{numberOfMessages}</td></tr>',
               '<tr><th>Number of Pending Messages</th><td>{numberOfPendingMessages}</td></tr>',
               '<tr><th>Total Message Bytes</th><td>{totalMessagesBytes}</td></tr>', '</table>', {})
      });
      this.callParent(arguments);
   },

   setActive : function(record) {
      this.active = record;
      this.update(record.data);
   },

   createToolbar : function() {
      var items = [], config = {};
      config.items = items;
      return Ext.create('widget.toolbar', config);
   },

   onLoad : function(store, records) {

   }

});