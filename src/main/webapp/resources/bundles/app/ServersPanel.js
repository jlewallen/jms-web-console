Ext.define('WebConsole.ServersPanel', {
   extend : 'Ext.panel.Panel',
   alias : 'widget.serverspanel',
   animCollapse : false,
   layout : 'fit',
   title : 'Servers',

   initComponent : function() {
      Ext.apply(this, {
         items : this.createView(),
      });
      this.addEvents('serverSelect');
      this.callParent(arguments);
   },

   createView : function() {
      var self = this;
      var store = new Ext.data.Store({
         id : 'address',
         model : 'Server',
         fields : [ 'address', 'url', 'name' ],
         proxy : {
            type : 'ajax',
            url : self.url,
            extraParams : {
               "_format" : "json"
            },
            reader : {
               type : 'json',
               root : 'servers'
            }
         },
         listeners : {
            load : this.onLoad,
            scope : this
         }
      });
      this.view = Ext.create('widget.dataview', {
         store : store,
         selModel : {
            mode : 'SINGLE',
            listeners : {
               scope : this,
               selectionchange : this.onSelectionChange
            }
         },
         listeners : {
            scope : this,
            contextmenu : this.onContextMenu,
            viewready : this.onViewReady
         },
         trackOver : true,
         cls : 'servers',
         itemSelector : '.servers-item',
         overItemCls : 'servers-item-hover',
         tpl : '<tpl for="."><div class="servers-item">{address}</div></tpl>'
      });
      this.view.store.load({});
      return this.view;
   },
   onLoad : function() {
      this.view.getSelectionModel().select(this.view.store.first());
   },

   onViewReady : function() {},

   onSelectionChange : function() {
      var selected = this.getSelectedItem();
      this.loadServer(selected);
   },

   loadServer : function(rec) {
      if (rec) {
         this.fireEvent('serverselect', this, rec);
      }
   },

   getSelectedItem : function() {
      return this.view.getSelectionModel().getSelection()[0] || false;
   },

   onContextMenu : function(view, index, el, event) {
      var menu = this.menu;
      event.stopEvent();
      menu.activeServer = view.store.getAt(index);
      menu.showAt(event.getXY());
   },

   onDestroy : function() {
      this.callParent(arguments);
      this.menu.destroy();
   }
});
