JMS Web Console
===============

1) Clone the repository.

2) Execute ./run[.cmd]

3) Point your browser to http://127.0.0.1:7171

By default the application will access 127.0.0.1:7676, you can include or
change that collection using the "servers" system property.
